#!/bin/sh


usage(){
    printf "disk-init v0.0.1
Copyright (C) 2021 Clayton Voges
License GNU GPL-3.0-or-later <https://gnu.org/licenses/gpl.html>
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to extent permitted by law.

Syntax: disk-init [options] disk(s)

Options:

-f, --file-system FS_TYPE   file system to format root partition(s)
-b, --bootable              bootable disk(s)
-r, --raid RAID_LVL         raid level to create RAID array
-e, --encrypted             encrypts the partition(s)"

    exit 2
}

options=$(getopt \
    -a -n disk_init \
    -o f:br:e \
    -l file-system:,bootable,raid:,encrypted -- "$@")
[ "$?" != "0" ] && usage

eval set -- "$options"

while :; do case "$1" in
    -f|--file-system)   FS_TYPE="$2"    ; shift ;;
    -b|--bootable)      BOOTABLE=1      ; shift ;;
    -r|--raid)          RAID_LVL="$2"   ; shift ;;
    -e|--encrypted)     ENCRYPTED=1     ; shift ;;
    --) shift; break ;;
    *) printf "Error: Unexpected option \"$1\"\n"; usage ;;
esac
done


disks="$@"
part_num=1
last_part="0%"
apk add parted

for_disks(){
    for disk in $disks; do
        eval $1
    done
}

for_disks "parted -s $disk mklabel gpt"

bootable_init(){
    parted -s $disk \
        unit s \
        mkpart boot fat32   0%      2MiB \
        mkpart efi  fat32   2MiB    100MiB \
        set 1 bios_grub on \
        set 2 esp on

    last_part="100MiB"
    mkfs.vfat "$disk$part_num"
    mkfs.vfat "$disk$(expr 1 + $part_num)"

    grub-install "$disk"
    grub-install \
        --target=x86_64-efi \
        --efi-directory=/boot/efi \
        --bootloader-id="Alpine"
    grub-mkconfig -o /boot/grub/grub.cfg
}

[ "${#disks}" != "1" ] && apk add mdadm
[ "$BOOTABLE" = "1" ] \
    && apk del syslinux \
    && apk add grub grub-bios grub-efi
    && for_disks "bootable_init" \
    && part_num=3

part_init(){
    parted -s $disk \
        mkpart swap linux-swap  $last_part  8GiB \
        mkpart root ext4        8GiB        100%

    mkswap "$disk$part_num"
    swapon "$disk$part_num"
}

for_disks "part_init"
part_num=$(expr 1 + $part_num)

btrfs_init(){
    apk add btrfs-progs btrfs-progs-extra
    mkfs.btrfs $1
    btrfs subvolume create <MOUNT POINT???>
}

fs_init(){
    eval "$1" "$disk$part_num"
}

case "$FS_TYPE" in
    btrfs)  for_disks "fs_init btrfs_init" ;;
    ext4)   for_disks "fs_init mkfs.ext4" ;;
    ext3)   for_disks "fs_init mkfs.ext3" ;;
    ext2)   for_disks "fs_init mkfs.ext2" ;;
    vfat)   for_disks "fs_init mkfs.vfat" ;;
esac

case "$RAID_LVL" in
    0)      raid0_init ;;
    1)      raid1_init ;;
    1c3)    raid1c3_init ;;
    1c4)    raid1c4_init ;;
    10)     raid10_init ;;
    5)      raid5_init ;;
    6)      raid6_init ;;
esac

[ $ENCRYPTED -eq 1 ] && apk add cryptsetup











































































